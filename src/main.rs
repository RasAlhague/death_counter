use inputbot::{KeybdKey::*, *};
use std::fs;
use app_dirs::{
    AppInfo,
    app_dir,
    AppDataType
};
use std::path::Path;
use std::fs::File;
use std::io::prelude::*;
use structopt::StructOpt;
use std::io::{self, Read};

/// Informations about the program for app_dirs
const APP_INFO: AppInfo = AppInfo{name: "death_counter", author: "bot"};

/// Simple config struct for getting the config text.
#[derive(StructOpt)]
#[structopt(about = "Short program for death counting. Written for streaming.", name = "death_counter")]
struct Config {
    /// The text which will be displayed with the count of deaths.
    #[structopt(short, long, help = "The text which will be displayed with the count of deaths.")]
    pub counter_text: Option<String>,
}

impl Config {
    /// just to be able to fast create an config struct, because from_args would be private
    pub fn new() -> Self {
        Config::from_args()
    }
}

fn get_gamename() -> String {
    let mut buffer = String::new();

    if let Err(why) = io::stdin().read_line(&mut buffer) {
        println!("Could not read from commandline! Err: {}", why);
    }
    else {
        println!("Loading deathcounter for {}!", buffer.trim());
    }

    buffer.trim().to_owned()
}

/// Adds a i32 number to the counter. I used i32 because then i can simple descrease the counter by adding -1.
fn add_to_counter(num: i32, gamename: &str) {
    let config = Config::new();

    // getting the counter display text
    let counter_text;
    if let Some(text) = config.counter_text {
        counter_text = text;
    }
    else {
        counter_text = String::from("Death");
    }

    let app_directory = app_dir(AppDataType::UserConfig, &APP_INFO, gamename);

    match app_directory {
        Ok(dir) => {
            let mut dir_str = dir.to_str().unwrap().to_owned();
            dir_str.push_str("/death_counter.txt");

            //println!("Path: {}", dir_str);

            if let Err(why) = create_file_if_not_exists(&dir_str, &counter_text) {
                println!("Could not change deathcounter: {}", why);

                return;
            }

            let count = match load_death_count(&dir_str) {
                Ok(c) => c,
                Err(why) => {
                    println!("Could not make app dir: {}", why);

                    return;
                },
            };

            let count = count + num;

            if let Err(why) = write_death_count(&dir_str, count, &counter_text) {
                println!("Could not change deathcounter: {}", why);

                return;
            }
            
            println!("Count: {}", count);
        },
        Err(why) => {
            println!("Could not make app dir: {}", why);
        },
    };
}

/// creates a death_counter_file if it did not exist.
fn create_file_if_not_exists(path: &str, counter_text: &str) -> Result<(), String> {
    if !Path::new(path).exists() {
        let mut file = match File::create(path) {
            Ok(f) => f,
            Err(why) => {
                return Err(format!("Could not create death_count file: {}", why));
            }
        };

        let text = format!("{}:0", counter_text);

        if let Err(why) = file.write_all(text.as_bytes()) {
            return Err(format!("Could not create death_count file: {}", why));
        };
    }

    Ok(())
}

fn load_death_count(path: &str) -> Result<i32, String> {
    let count_string = match fs::read_to_string(path) {
        Ok(data) => data, 
        Err(why) => {
            return Err(format!("Could not make app dir: {}", why));
        },
    };

    let split_string = count_string.split(':').collect::<Vec<&str>>();
    
    Ok(split_string[1].parse().unwrap())
}

fn write_death_count(path: &str, death_count: i32, counter_text: &str) -> Result<(), String> {
    let text = format!("{}:{}", counter_text, death_count);

    if let Err(why) = fs::write(path, text.as_bytes()) {
        return Err(format!("Could not write death_count file: {}", why));
    } 

    Ok(())
}

fn main() {
    println!("Please enter the game name:");

    let gamename = get_gamename();
    let gamename2 = gamename.clone();

    match app_dir(AppDataType::UserConfig, &APP_INFO, &gamename) {
        Ok(path_buf) => println!("Deathcounter path: {}/death_counter.txt", path_buf.display()),
        Err(why) => println!("Could not find app dir: {}", why),
    };
    
    Numpad2Key.bind(move|| add_to_counter(1, &gamename));
    Numpad1Key.bind(move|| add_to_counter(-1, &gamename2));

    handle_input_events();
}